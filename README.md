# Coding Challenge - Sequences

You will be required to write a program to process a sequence of instructions. Each instruction is
represented by a digit from 0-4, and the digits are glued together into a string and processed in
turn. So, for example:

`11203411`

could be a valid sequence of instructions. The slight complication is that some of the instructions
modify the sequence itself.

Each digit has a specific meaning as follows:

|     |                                                                                                               |
| --- | ------------------------------------------------------------------------------------------------------------- |
| 0   | stop processing and *output the final sequence*                                                               |
| 1   | do nothing, i.e. continue to the next instruction in the sequence                                             |
| 2   | increment the instruction that was previously processed, modulo 5 (i.e. 4 becomes 0 when incremented, not 5)  |
| 3   | decrement the instruction that was previously processed, modulo 5 (i.e. 0 becomes 4 when decremented, not -1) |
| 4   | reverse direction, i.e. change processing from left-to-right to right-to-left, or vice versa                  |

Processing starts from the left-most digit and initially proceeds left-to-right. If processing
reaches either end of the sequence (left or right), it wraps around and continues from the other
end. (NB. The instruction "previously processed" does \*not\* necessarily mean the instruction to
the left of the current instruction - because processing can be either left-to-right or
right-to-left.)

Near the bottom of the page is a full, worked example for illustrative purposes.

## The Question

Write a program to determine the output produced by the starting sequence: `1212121`.

(Hint: it takes 58 iterations to get there.)


## Getting Started

- You'll need a version 11 Java Development Kit (JDK) installed.  A good source is
[here](https://adoptopenjdk.net/?variant=openjdk11&jvmVariant=hotspot).

- Clone the repository: `git clone git@bitbucket.org:ghyston/java-scrabble-stats-exercise.git` and
create a new Git branch to do your work on.

- Open the project using IntelliJ (we'd recommend you install this via the [Jetbrains Toolbox
App](https://www.jetbrains.com/toolbox/app/))

- The starting point for this challenge is in the class
[com.ghyston.training.sequences.Main](./src/main/java/com/ghyston/training/sequences/Main.java). 
There are several ways to run this application:

    - Navigate to this class in IntelliJ, right click it and select `Run 'Main.main()'` or `Debug
    'Main.main()'`

    - Choose the 'Main' configuration in the IntelliJ toolbar and use the green buttons to the right
    to either run or debug.

    - Run `./mvnw spring-boot:run` from a terminal in the project directory (omit the leading '`./`'
    if using cmd.exe)


- Get cracking on the challenge!


## When You're Finished

Please commit and push your work to the repository. You can then create a review in
[Upsource](https://upsource.ghyston.com/csharpcodingchallenges) and ask your mentor to look at it.
Once the review is completed online, your reviewer will go through the review comments and work on
them with you.

If you have any questions about Upsource please ask away! We use this tool for many of our reviews,
so it is good to be familiar with it.

*The review comments will be particularly picky this time so please expect a lot of suggestions!*

## Worked example

The starting sequence `121` takes 19 iterations to produce `203` - see below. The instruction
*about to be processed* is in brackets, and the direction of processing is indicated to the right.

```
 0: [1]2 1  >
 1:  1[2]1  >
 2:  2 2[1] >
 3: [2]2 1  >
 4:  2[2]2  >
 5:  3 2[2] >
 6: [3]3 2  >
 7:  3[3]1  >
 8:  2 3[1] >
 9: [2]3 1  >
10:  2[3]2  >
11:  1 3[2] >
12: [1]4 2  >
13:  1[4]2  >
14: [1]4 2  <
15:  1 4[2] <
16:  2[4]2  <
17:  2 4[2] >
18: [2]0 2  >
19:  2[0]3  >
```