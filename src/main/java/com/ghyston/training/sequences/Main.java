package com.ghyston.training.sequences;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        if (args.length == 1) {
            processSequence(args[0]);
        } else {
            processSequencesInteractively();
        }
    }


    private static void processSequencesInteractively() {
        Scanner inputReader = new Scanner(System.in);
        while(true) {
            System.out.print("\nPlease enter a sequence, or an empty line to exit\n> ");
            String input = inputReader.nextLine().trim();
            if (input.isEmpty()) {
                break;
            }
            processSequence(input);
        }
    }

    private static void processSequence(String sequence) {
        System.out.println("Processing sequence: \"" + sequence + "\"");
        try {
            SequenceProcessor processor = new SequenceProcessor(sequence);
            String output = processor.run();
            System.out.println("Output: \"" + output + "\"");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
