package com.ghyston.training.sequences;

public class SequenceProcessor {

    private int[] seq;
    private int position;
    private int direction;

    public SequenceProcessor(String sequence) {
        if (sequence == null || !sequence.matches("[0-4]*")) {
            throw new IllegalArgumentException("sequence must be a string consisting of the digits 0-4 only");
        }

        seq = SequenceUtils.sequenceStringToArray(sequence);
        position = 0;
        direction = 1;
    }

    public String run() {
        while(!isFinished()) {
            step();
        }

        return SequenceUtils.sequenceArrayToString(seq);
    }

    private boolean isFinished() {
        // TODO!
        return true;
    }

    private void step() {
        int currentInstruction = seq[position];

        switch (currentInstruction) {
            case 0:
                // TODO: do something
                break;
            case 1:
                // TODO: do something
                break;
            case 2:
                // TODO: do something
                break;
            case 3:
                // TODO: do something
                break;
            case 4:
                // TODO: do something
                break;
            default:
                throw new IllegalStateException("Can't process instruction " + currentInstruction);
        }
    }

}
