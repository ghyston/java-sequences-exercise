package com.ghyston.training.sequences;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SequenceUtils {
    public static String sequenceArrayToString(int[] seq) {
        return Arrays.stream(seq).mapToObj(String::valueOf).collect(Collectors.joining());
    }

    public static int[] sequenceStringToArray(String sequence) {
        return sequence.chars().map(c -> c - '0').toArray();
    }
}
