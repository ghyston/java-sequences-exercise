package com.ghyston.training.sequences;

import org.junit.Ignore;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SequenceProcessorTest {

    @Ignore // TODO: fix and enable!
    @Test
    public void emptySequence_returnsEmpty() {
        assertThat(new SequenceProcessor("").run()).isEmpty();
    }

    @Ignore // TODO: fix and enable!
    @Test
    public void exampleSequence_givesExpectedAnswer() {
        assertThat(new SequenceProcessor("121").run()).isEqualTo("203");
    }

}